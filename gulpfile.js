var gulp         = require("gulp"),
    browserSync  = require("browser-sync").create(),
    sass         = require("gulp-sass"),
    postcss      = require("gulp-postcss"),
    autoprefixer = require("autoprefixer"),
    cssnano      = require("cssnano"),
    sourcemaps   = require("gulp-sourcemaps"),
    concatCss    = require('gulp-concat-css');


var paths = {
    base: {
        src: "src/lab3/public/."
    },
    styles: {
        src: "src/lab3/src/*.scss",
        dest: "src/lab3/public/"
    },
    script: {
        src: "src/la3/public/*.js"
    },
    html: {
        src: "src/lab3/public/*.html"
    }
};

function style() {
    return (
        gulp
            .src(paths.styles.src)
            .pipe(sourcemaps.init())
            .pipe(sass())
            .on("error", sass.logError)
            .pipe(postcss([autoprefixer(), cssnano()]))
            .pipe(sourcemaps.write())
            .pipe(gulp.dest(paths.styles.dest))
            .pipe(browserSync.stream())
    );
}


function watch() {
    browserSync.init({
        server: {
            baseDir: paths.base.src
        }
    });
    gulp.watch(paths.styles.src, style);
    gulp.watch(paths.html.src).on('change', browserSync.reload);
    gulp.watch(paths.script.src).on('change', browserSync.reload);
}

exports.watch = watch
