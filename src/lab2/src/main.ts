document.documentElement.ondragstart = () => false;
const container = document.getElementById('container');
const button = document.getElementById('run-button') as HTMLButtonElement;

const bezierContainer = document.getElementById('bezier-container');
const bezierTemplate = document.getElementById('bezier');

const pointTemplate = document.getElementById('point');

if (pointTemplate && pointTemplate.parentNode) {
    pointTemplate.parentNode.removeChild(pointTemplate);
}

let tPathTemplate = document.getElementById("t-path-template") as HTMLElement;
if (tPathTemplate && tPathTemplate.parentNode) {
    tPathTemplate.parentNode.removeChild(tPathTemplate);
}

if (!container) {
    throw new Error('Документ не загружен!');
}

container.onload = () => drawPoints(3);

type PointType = {
    x: number;
    y: number;
    point: HTMLElement;
}
type Points = PointType[];

type SubPointType = {
    x: number;
    y: number;
};
type SubPoints = SubPointType[];

type Resolution = {
    width: number;
    height: number;
}

let points: Points = [];

function handlePointsInput() {
    const pointsInput = document.getElementById('point-number') as HTMLInputElement;
    let pointsNumber = Number(pointsInput.value);

    if (pointsNumber < 3) {
        pointsInput.value = "3";
        pointsNumber = 3;
    }

    drawPoints(pointsNumber);
}

function drawPoints(pointsNumber: number) {
    const {width, height} = getResolution();

    if (points.length > pointsNumber) {
        for (let i = pointsNumber; i < points.length; i++) {
            points[i].point.parentNode?.removeChild(points[i].point);
        }
        points.splice(-(points.length-pointsNumber));
    } else {
        for (let i = points.length; i < pointsNumber; i++) {
            const x = Number((Math.random() * (width - 40) + 40).toFixed());
            const y = Number((Math.random() * (height - 40) + 40).toFixed());
            const point = pointTemplate?.cloneNode(true) as HTMLElement;

            points.push({x, y, point});
            const pointText = point.getElementsByTagName('text')[0].firstChild;
            if (pointText) {
                // @ts-ignore
                pointText.data = (i+1).toString();
            }

            setPointCoords(point, x, y);
            setPointHandler(point, i);
            container?.appendChild(point);
        }
    }

    drawPath();
}

function setPointCoords(point: HTMLElement, x: number, y: number) {
    point.setAttribute('x', (x - 20).toString());
    point.setAttribute('y', (y - 20).toString());
}

function getResolution(): Resolution {
    const width = window.innerWidth
        || document.documentElement.clientWidth
        || document.body.clientWidth;

    const height = window.innerHeight
        || document.documentElement.clientHeight
        || document.body.clientHeight;
    return ({width, height})
}

function setPointHandler(point: HTMLElement, i: number) {
    let circle = point.getElementsByTagName('circle')[0];
    circle.onmousedown = () => {
        document.onmousemove = (e) => {
            let x = e.pageX;
            let y = e.pageY;
            const {width, height} = getResolution();

            if (x < 20) {
                x = 20;
            }
            if (x > width - 20) {
                x = width - 20;
            }
            if (y > height - 20) {
                y = height - 20;
            }
            if (y < 20) {
                y = 20;
            }

            points[i].x = x;
            points[i].y = y;
            setPointCoords(point, points[i].x, points[i].y);
            drawPath();
        };

        document.onmouseup = () => document.onmousemove = document.onmouseup = null;
        return false;
    }
}

function drawPath() {
    const controlPath = document.getElementById("control-path");

    let controlPathD = `M ${points[0].x} ${points[0].y} L`;

    for (let i = 1; i < points.length; i++) {
        controlPathD += ` ${points[i].x} ${points[i].y}`;
    }

    if (!controlPath) {
        return;
    }

    controlPath.setAttribute('d', controlPathD);

    while (bezierContainer && bezierContainer.lastChild && bezierContainer.firstChild) {
        bezierContainer.removeChild(bezierContainer.lastChild);
    }

    setTimeout(() => {
        const xy: SubPoints = [];
        for (let i = 0; i <= 1; i += 0.01) {
            drawBezierCurve(points, xy, i);
        }
        const bezier = bezierTemplate?.cloneNode() as HTMLElement;
        bezier?.setAttribute('d', `M ${xy[xy.length-1].x} ${xy[xy.length-1].y} L ${points[points.length-1].x} ${points[points.length-1].y}`);
        bezierContainer?.appendChild(bezier);
    });
}

function drawConnections(points: SubPoints, t: number) {
    let path = document.getElementById('t-' + points.length);

    if (!path) {
        path = tPathTemplate.cloneNode(true) as HTMLElement;
        path.setAttribute('stroke', ["red","green","orange"][points.length % 3]);
        path.setAttribute('id', 't-' + points.length);
        container?.appendChild(path);
    }

    let subPoints: SubPoints = [];
    let x = points[0].x + (points[1].x - points[0].x)*t;
    let y = points[0].y + (points[1].y - points[0].y)*t;

    let tPathD = `M ${x} ${y} L`;
    subPoints.push({x, y});

    for(let i=1; i < points.length-1; i++) {
        let x = points[i].x + (points[i+1].x - points[i].x) * t;
        let y = points[i].y + (points[i+1].y - points[i].y) * t;
        subPoints.push({x, y});

        tPathD += ` ${x} ${y} `;
    }

    if (points.length <= 3) {
        const m = document.getElementById('marker');
        let mx, my;
        if (t === 1) {
            mx = -10, my = -10;
        } else {
            mx = subPoints[0].x + (subPoints[1].x - subPoints[0].x) * t;
            my = subPoints[0].y + (subPoints[1].y - subPoints[0].y) * t;
        }
        m?.setAttribute('cx', mx.toString());
        m?.setAttribute('cy', my.toString());
        path.setAttribute('stroke-width', '1');
    }

    path.setAttribute('d', tPathD);

    if (subPoints.length > 2){
        drawConnections(subPoints, t);
    }
}

function drawBezierCurve(points: SubPoints, xy: SubPoints, t: number) {
    const bezier = bezierTemplate?.cloneNode() as HTMLElement;

    let subPoints: SubPoints = [];
    let x = points[0].x + (points[1].x - points[0].x)*t;
    let y = points[0].y + (points[1].y - points[0].y)*t;
    subPoints.push({x, y});

    for(let i=1; i < points.length-1; i++) {
        let x = points[i].x + (points[i+1].x - points[i].x) * t;
        let y = points[i].y + (points[i+1].y - points[i].y) * t;
        subPoints.push({x, y});
    }

    if (points.length <= 3) {
        let mx, my;
        if (t === 1) {
            mx = -10, my = -10;
        } else {
            mx = subPoints[0].x + (subPoints[1].x - subPoints[0].x) * t;
            my = subPoints[0].y + (subPoints[1].y - subPoints[0].y) * t;
            xy.push({x: mx, y: my});
        }

        if (xy[xy.length-1] && xy[xy.length-2]) {
            bezier?.setAttribute('d', `M ${xy[xy.length-2].x} ${xy[xy.length-2].y} L ${xy[xy.length-1].x} ${xy[xy.length-1].y}`);
        }
    }

    bezierContainer?.appendChild(bezier);

    if (subPoints.length > 2) {
        drawBezierCurve(subPoints, xy, t);
    }
}

let t = 0;
let timer: number;

function animate(complete: Function) {
    timer = window.setInterval(function() {
        if (t > 1) {
            t = 1;
        }

        button.innerHTML = 'Пауза';

        drawConnections(points, t);

        if (t == 1) {
            window.clearInterval(timer);
            timer = 0;
            complete && complete();
            return;
        }

        t += 0.005;
    }, 30);
}

function animationDone() {
    for(let i = 1; i <= points.length; i++) {
        let path = document.getElementById('t-' + i);
        path && container?.removeChild(path);
    }
    button.innerHTML = 'Визуализировать построение';
    t = 0;
}

function onAnimate() {
    if (timer) {
        window.clearInterval(timer);
        button.innerHTML = 'Продолжить';
        timer = 0;
        return;
    }

    animate(animationDone)
}

const toolbox = document.getElementById('toolbox') as HTMLElement;

dragToolbox(toolbox);

function dragToolbox(toolbox: HTMLElement) {
    let pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
    const toolboxHeader = document.getElementById(toolbox.id + "-header");
    if (toolboxHeader) {
        toolboxHeader.onmousedown = dragMouseDown;
    } else {
        toolbox.onmousedown = dragMouseDown;
    }

    function dragMouseDown(e: MouseEvent) {
        e = e || window.event;
        e.preventDefault();
        pos3 = e.clientX;
        pos4 = e.clientY;
        document.onmouseup = closeDragElement;
        document.onmousemove = elementDrag;
    }

    function elementDrag(e: MouseEvent) {
        e = e || window.event;
        e.preventDefault();
        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;
        toolbox.style.top = (toolbox.offsetTop - pos2) + "px";
        toolbox.style.left = (toolbox.offsetLeft - pos1) + "px";
    }

    function closeDragElement() {
        document.onmouseup = null;
        document.onmousemove = null;
    }
}
