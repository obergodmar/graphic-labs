"use strict";
document.documentElement.ondragstart = function () { return false; };
var container = document.getElementById('container');
var button = document.getElementById('run-button');
var bezierContainer = document.getElementById('bezier-container');
var bezierTemplate = document.getElementById('bezier');
var pointTemplate = document.getElementById('point');
if (pointTemplate && pointTemplate.parentNode) {
    pointTemplate.parentNode.removeChild(pointTemplate);
}
var tPathTemplate = document.getElementById("t-path-template");
if (tPathTemplate && tPathTemplate.parentNode) {
    tPathTemplate.parentNode.removeChild(tPathTemplate);
}
if (!container) {
    throw new Error('Документ не загружен!');
}
container.onload = function () { return drawPoints(3); };
var points = [];
function handlePointsInput() {
    var pointsInput = document.getElementById('point-number');
    var pointsNumber = Number(pointsInput.value);
    if (pointsNumber < 3) {
        pointsInput.value = "3";
        pointsNumber = 3;
    }
    drawPoints(pointsNumber);
}
function drawPoints(pointsNumber) {
    var _a;
    var _b = getResolution(), width = _b.width, height = _b.height;
    if (points.length > pointsNumber) {
        for (var i = pointsNumber; i < points.length; i++) {
            (_a = points[i].point.parentNode) === null || _a === void 0 ? void 0 : _a.removeChild(points[i].point);
        }
        points.splice(-(points.length - pointsNumber));
    }
    else {
        for (var i = points.length; i < pointsNumber; i++) {
            var x = Number((Math.random() * (width - 40) + 40).toFixed());
            var y = Number((Math.random() * (height - 40) + 40).toFixed());
            var point = pointTemplate === null || pointTemplate === void 0 ? void 0 : pointTemplate.cloneNode(true);
            points.push({ x: x, y: y, point: point });
            var pointText = point.getElementsByTagName('text')[0].firstChild;
            if (pointText) {
                // @ts-ignore
                pointText.data = (i + 1).toString();
            }
            setPointCoords(point, x, y);
            setPointHandler(point, i);
            container === null || container === void 0 ? void 0 : container.appendChild(point);
        }
    }
    drawPath();
}
function setPointCoords(point, x, y) {
    point.setAttribute('x', (x - 20).toString());
    point.setAttribute('y', (y - 20).toString());
}
function getResolution() {
    var width = window.innerWidth
        || document.documentElement.clientWidth
        || document.body.clientWidth;
    var height = window.innerHeight
        || document.documentElement.clientHeight
        || document.body.clientHeight;
    return ({ width: width, height: height });
}
function setPointHandler(point, i) {
    var circle = point.getElementsByTagName('circle')[0];
    circle.onmousedown = function () {
        document.onmousemove = function (e) {
            var x = e.pageX;
            var y = e.pageY;
            var _a = getResolution(), width = _a.width, height = _a.height;
            if (x < 20) {
                x = 20;
            }
            if (x > width - 20) {
                x = width - 20;
            }
            if (y > height - 20) {
                y = height - 20;
            }
            if (y < 20) {
                y = 20;
            }
            points[i].x = x;
            points[i].y = y;
            setPointCoords(point, points[i].x, points[i].y);
            drawPath();
        };
        document.onmouseup = function () { return document.onmousemove = document.onmouseup = null; };
        return false;
    };
}
function drawPath() {
    var controlPath = document.getElementById("control-path");
    var controlPathD = "M " + points[0].x + " " + points[0].y + " L";
    for (var i = 1; i < points.length; i++) {
        controlPathD += " " + points[i].x + " " + points[i].y;
    }
    if (!controlPath) {
        return;
    }
    controlPath.setAttribute('d', controlPathD);
    while (bezierContainer && bezierContainer.lastChild && bezierContainer.firstChild) {
        bezierContainer.removeChild(bezierContainer.lastChild);
    }
    setTimeout(function () {
        var xy = [];
        for (var i = 0; i <= 1; i += 0.01) {
            drawBezierCurve(points, xy, i);
        }
        var bezier = bezierTemplate === null || bezierTemplate === void 0 ? void 0 : bezierTemplate.cloneNode();
        bezier === null || bezier === void 0 ? void 0 : bezier.setAttribute('d', "M " + xy[xy.length - 1].x + " " + xy[xy.length - 1].y + " L " + points[points.length - 1].x + " " + points[points.length - 1].y);
        bezierContainer === null || bezierContainer === void 0 ? void 0 : bezierContainer.appendChild(bezier);
    });
}
function drawConnections(points, t) {
    var path = document.getElementById('t-' + points.length);
    if (!path) {
        path = tPathTemplate.cloneNode(true);
        path.setAttribute('stroke', ["red", "green", "orange"][points.length % 3]);
        path.setAttribute('id', 't-' + points.length);
        container === null || container === void 0 ? void 0 : container.appendChild(path);
    }
    var subPoints = [];
    var x = points[0].x + (points[1].x - points[0].x) * t;
    var y = points[0].y + (points[1].y - points[0].y) * t;
    var tPathD = "M " + x + " " + y + " L";
    subPoints.push({ x: x, y: y });
    for (var i = 1; i < points.length - 1; i++) {
        var x_1 = points[i].x + (points[i + 1].x - points[i].x) * t;
        var y_1 = points[i].y + (points[i + 1].y - points[i].y) * t;
        subPoints.push({ x: x_1, y: y_1 });
        tPathD += " " + x_1 + " " + y_1 + " ";
    }
    if (points.length <= 3) {
        var m = document.getElementById('marker');
        var mx = void 0, my = void 0;
        if (t === 1) {
            mx = -10, my = -10;
        }
        else {
            mx = subPoints[0].x + (subPoints[1].x - subPoints[0].x) * t;
            my = subPoints[0].y + (subPoints[1].y - subPoints[0].y) * t;
        }
        m === null || m === void 0 ? void 0 : m.setAttribute('cx', mx.toString());
        m === null || m === void 0 ? void 0 : m.setAttribute('cy', my.toString());
        path.setAttribute('stroke-width', '1');
    }
    path.setAttribute('d', tPathD);
    if (subPoints.length > 2) {
        drawConnections(subPoints, t);
    }
}
function drawBezierCurve(points, xy, t) {
    var bezier = bezierTemplate === null || bezierTemplate === void 0 ? void 0 : bezierTemplate.cloneNode();
    var subPoints = [];
    var x = points[0].x + (points[1].x - points[0].x) * t;
    var y = points[0].y + (points[1].y - points[0].y) * t;
    subPoints.push({ x: x, y: y });
    for (var i = 1; i < points.length - 1; i++) {
        var x_2 = points[i].x + (points[i + 1].x - points[i].x) * t;
        var y_2 = points[i].y + (points[i + 1].y - points[i].y) * t;
        subPoints.push({ x: x_2, y: y_2 });
    }
    if (points.length <= 3) {
        var mx = void 0, my = void 0;
        if (t === 1) {
            mx = -10, my = -10;
        }
        else {
            mx = subPoints[0].x + (subPoints[1].x - subPoints[0].x) * t;
            my = subPoints[0].y + (subPoints[1].y - subPoints[0].y) * t;
            xy.push({ x: mx, y: my });
        }
        if (xy[xy.length - 1] && xy[xy.length - 2]) {
            bezier === null || bezier === void 0 ? void 0 : bezier.setAttribute('d', "M " + xy[xy.length - 2].x + " " + xy[xy.length - 2].y + " L " + xy[xy.length - 1].x + " " + xy[xy.length - 1].y);
        }
    }
    bezierContainer === null || bezierContainer === void 0 ? void 0 : bezierContainer.appendChild(bezier);
    if (subPoints.length > 2) {
        drawBezierCurve(subPoints, xy, t);
    }
}
var t = 0;
var timer;
function animate(complete) {
    timer = window.setInterval(function () {
        if (t > 1) {
            t = 1;
        }
        button.innerHTML = 'Пауза';
        drawConnections(points, t);
        if (t == 1) {
            window.clearInterval(timer);
            timer = 0;
            complete && complete();
            return;
        }
        t += 0.005;
    }, 30);
}
function animationDone() {
    for (var i = 1; i <= points.length; i++) {
        var path = document.getElementById('t-' + i);
        path && (container === null || container === void 0 ? void 0 : container.removeChild(path));
    }
    button.innerHTML = 'Визуализировать построение';
    t = 0;
}
function onAnimate() {
    if (timer) {
        window.clearInterval(timer);
        button.innerHTML = 'Продолжить';
        timer = 0;
        return;
    }
    animate(animationDone);
}
var toolbox = document.getElementById('toolbox');
dragToolbox(toolbox);
function dragToolbox(toolbox) {
    var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
    var toolboxHeader = document.getElementById(toolbox.id + "-header");
    if (toolboxHeader) {
        toolboxHeader.onmousedown = dragMouseDown;
    }
    else {
        toolbox.onmousedown = dragMouseDown;
    }
    function dragMouseDown(e) {
        e = e || window.event;
        e.preventDefault();
        pos3 = e.clientX;
        pos4 = e.clientY;
        document.onmouseup = closeDragElement;
        document.onmousemove = elementDrag;
    }
    function elementDrag(e) {
        e = e || window.event;
        e.preventDefault();
        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;
        toolbox.style.top = (toolbox.offsetTop - pos2) + "px";
        toolbox.style.left = (toolbox.offsetLeft - pos1) + "px";
    }
    function closeDragElement() {
        document.onmouseup = null;
        document.onmousemove = null;
    }
}
