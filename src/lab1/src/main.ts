const object = document.getElementById('scale-object');

handleAnimation();
handleAxis();
handleGrid();
restoreObject();
sizeObject();

function matrixArrayToCssMatrix(array: number[]): string {
    return 'matrix3d(' + array.join(',') + ')';
}

function sizeObject() {
    const {value: width} = document.getElementById('width') as HTMLInputElement;
    const {value: height} = document.getElementById('height') as HTMLInputElement;

    const w = Number(width);
    const h = Number(height);

    if (object) {
        object.style.width = `${w}px`;
        object.style.height = `${h}px`;
    }
}

function scaleObject() {
    const {value: width} = document.getElementById('scale-width') as HTMLInputElement;
    const {value: height} = document.getElementById('scale-height') as HTMLInputElement;
    
    const d = 1;
    const w = Number(width);
    const h = Number(height);

    let scaleMatrix = [
        w,    0,    0,   0,
        0,    h,    0,   0,
        0,    0,    d,   0,
        0,    0,    0,   1
    ];

    if (object) {
        object.style.transform = matrixArrayToCssMatrix(scaleMatrix);
    }
}

function restoreObject() {
    const width = document.getElementById('scale-width') as HTMLInputElement;
    const height = document.getElementById('scale-height') as HTMLInputElement;

    width.value = '1';
    height.value = '1';

    const normalSize = [
        1,    0,    0,   0,
        0,    1,    0,   0,
        0,    0,    1,   0,
        0,    0,    0,   1
    ];

    if (object) {
        object.style.transform = matrixArrayToCssMatrix(normalSize);
    }
}

function handleAnimation() {
    const checkbox = document.getElementById('animation') as HTMLInputElement;
    if (!checkbox || !object) { 
        return;
    }

    const speed = '0.5s';

    if (checkbox.checked) {
        object.style.transition = `transform ${speed}, width ${speed}, height ${speed}`;
    } else {
        object.style.transition = ''
    }
}

function handleGrid() {
    const checkbox = document.getElementById('grid') as HTMLInputElement;
    if (!checkbox) { 
        return;
    }

    if (checkbox.checked) {
        document.body.style.backgroundImage = 'linear-gradient(transparent 11px, rgba(220,220,200,.8) 12px, transparent 12px), linear-gradient(90deg, transparent 11px, rgba(220,220,200,.8) 12px, transparent 12px)';
        document.body.style.backgroundSize = '100% 12px, 12px 100%';
    } else {
        document.body.style.backgroundImage = '';
        document.body.style.backgroundSize = '';
    }
}

function handleAxis() {
    const checkbox = document.getElementById('axis') as HTMLInputElement;
    const axisX = document.getElementById('axis-x');
    const axisY = document.getElementById('axis-y');
    if (!checkbox || !axisX || !axisY) { 
        return;
    }

    if (checkbox.checked) {
        axisX.style.display = 'block';
        axisY.style.display = 'block';
    } else {
        axisX.style.display = 'none';
        axisY.style.display = 'none';
    }
}

const toolbox = document.getElementById('toolbox') as HTMLElement;

dragToolbox(toolbox);

function dragToolbox(toolbox: HTMLElement) {
    let pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
    const toolboxHeader = document.getElementById(toolbox.id + "-header");
    if (toolboxHeader) {
        toolboxHeader.onmousedown = dragMouseDown;
    } else {
        toolbox.onmousedown = dragMouseDown;
    }

    function dragMouseDown(e: MouseEvent) {
        e = e || window.event;
        e.preventDefault();
        pos3 = e.clientX;
        pos4 = e.clientY;
        document.onmouseup = closeDragElement;
        document.onmousemove = elementDrag;
    }

    function elementDrag(e: MouseEvent) {
        e = e || window.event;
        e.preventDefault();
        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;
        toolbox.style.top = (toolbox.offsetTop - pos2) + "px";
        toolbox.style.left = (toolbox.offsetLeft - pos1) + "px";
    }

    function closeDragElement() {
        document.onmouseup = null;
        document.onmousemove = null;
    }
}