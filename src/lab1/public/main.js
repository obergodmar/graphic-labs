"use strict";
var object = document.getElementById('scale-object');
handleAnimation();
handleAxis();
handleGrid();
restoreObject();
sizeObject();
function matrixArrayToCssMatrix(array) {
    return 'matrix3d(' + array.join(',') + ')';
}
function sizeObject() {
    var width = document.getElementById('width').value;
    var height = document.getElementById('height').value;
    var w = Number(width);
    var h = Number(height);
    if (object) {
        object.style.width = w + "px";
        object.style.height = h + "px";
    }
}
function scaleObject() {
    var width = document.getElementById('scale-width').value;
    var height = document.getElementById('scale-height').value;
    var d = 1;
    var w = Number(width);
    var h = Number(height);
    var scaleMatrix = [
        w, 0, 0, 0,
        0, h, 0, 0,
        0, 0, d, 0,
        0, 0, 0, 1
    ];
    if (object) {
        object.style.transform = matrixArrayToCssMatrix(scaleMatrix);
    }
}
function restoreObject() {
    var width = document.getElementById('scale-width');
    var height = document.getElementById('scale-height');
    width.value = '1';
    height.value = '1';
    var normalSize = [
        1, 0, 0, 0,
        0, 1, 0, 0,
        0, 0, 1, 0,
        0, 0, 0, 1
    ];
    if (object) {
        object.style.transform = matrixArrayToCssMatrix(normalSize);
    }
}
function handleAnimation() {
    var checkbox = document.getElementById('animation');
    if (!checkbox || !object) {
        return;
    }
    var speed = '0.5s';
    if (checkbox.checked) {
        object.style.transition = "transform " + speed + ", width " + speed + ", height " + speed;
    }
    else {
        object.style.transition = '';
    }
}
function handleGrid() {
    var checkbox = document.getElementById('grid');
    if (!checkbox) {
        return;
    }
    if (checkbox.checked) {
        document.body.style.backgroundImage = 'linear-gradient(transparent 11px, rgba(220,220,200,.8) 12px, transparent 12px), linear-gradient(90deg, transparent 11px, rgba(220,220,200,.8) 12px, transparent 12px)';
        document.body.style.backgroundSize = '100% 12px, 12px 100%';
    }
    else {
        document.body.style.backgroundImage = '';
        document.body.style.backgroundSize = '';
    }
}
function handleAxis() {
    var checkbox = document.getElementById('axis');
    var axisX = document.getElementById('axis-x');
    var axisY = document.getElementById('axis-y');
    if (!checkbox || !axisX || !axisY) {
        return;
    }
    if (checkbox.checked) {
        axisX.style.display = 'block';
        axisY.style.display = 'block';
    }
    else {
        axisX.style.display = 'none';
        axisY.style.display = 'none';
    }
}
var toolbox = document.getElementById('toolbox');
dragToolbox(toolbox);
function dragToolbox(toolbox) {
    var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
    var toolboxHeader = document.getElementById(toolbox.id + "-header");
    if (toolboxHeader) {
        toolboxHeader.onmousedown = dragMouseDown;
    }
    else {
        toolbox.onmousedown = dragMouseDown;
    }
    function dragMouseDown(e) {
        e = e || window.event;
        e.preventDefault();
        pos3 = e.clientX;
        pos4 = e.clientY;
        document.onmouseup = closeDragElement;
        document.onmousemove = elementDrag;
    }
    function elementDrag(e) {
        e = e || window.event;
        e.preventDefault();
        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;
        toolbox.style.top = (toolbox.offsetTop - pos2) + "px";
        toolbox.style.left = (toolbox.offsetLeft - pos1) + "px";
    }
    function closeDragElement() {
        document.onmouseup = null;
        document.onmousemove = null;
    }
}
