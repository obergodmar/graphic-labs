const { app, BrowserWindow, Menu } = require('electron');
const path = require('path');
const {format} = require('url');
let mainWindow;

const template = [
    {
        label: 'Файл',
        submenu: [{ role: 'quit' }]
    },
    {
        label: 'Вид',
        submenu: [
            { role: 'reload' },
            { role: 'togglefullscreen' }
        ]
    },
    {
        label: 'Окно',
        submenu: [
            { role: 'minimize' },
            { role: 'close' }
        ]
    }
];

const menu = Menu.buildFromTemplate(template);

function createWindow() {
    mainWindow = new BrowserWindow({
        minWidth: 550,
        minHeight: 420,
        resizable: true,
        autoHideMenuBar: true
    });

    Menu.setApplicationMenu(menu);

    mainWindow.loadURL(format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file',
        slashes: true
    }));

    mainWindow.on('closed', () => mainWindow = null);
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('activate', () => {
    if (mainWindow === null) {
        createWindow();
    }
});