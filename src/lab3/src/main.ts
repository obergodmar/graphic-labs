$(document).ready(function() {
    const scene = new THREE.Scene();
    const camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.1, 1000);
    const cameraControls = new THREE.OrbitControls(camera);
    const renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
    renderer.setSize(window.innerWidth, window.innerHeight);
    const axisHelper = new THREE.AxisHelper(30);
    scene.add(axisHelper);

    $(window).on('resize', function() {
        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();
        renderer.setSize(window.innerWidth, window.innerHeight);
        renderScene();
    });

    camera.position.x = -60;
    camera.position.y = 50;
    camera.position.z = -40;
    camera.lookAt(new THREE.Vector3(20,0,15));
    camera.updateProjectionMatrix();
    $(cameraControls).on('change', renderScene);
    cameraControls.target = new THREE.Vector3(20,0,15);

    let bezierCurveDivisions = 10;
    let bezierSurface, bezierSurfaceGeometry, bezierSurfaceMaterial;

    let bezierControlPoints = [
        [
            new THREE.Vector3(-10, 10, 0),
            new THREE.Vector3(0, 7, 0),
            new THREE.Vector3(15, 3, 0),
            new THREE.Vector3(30, 8, 0)
        ],
        [
            new THREE.Vector3(-10, 0, 10),
            new THREE.Vector3(-5, 15, 10),
            new THREE.Vector3(20, 10, 10),
            new THREE.Vector3(30, 5, 10)
        ],
        [
            new THREE.Vector3(-10, 5, 20),
            new THREE.Vector3(-5,-10, 20),
            new THREE.Vector3(10, 10, 20),
            new THREE.Vector3(30, 0, 20)
        ],
        [
            new THREE.Vector3(-10, 4, 30),
            new THREE.Vector3(-5, 8, 30),
            new THREE.Vector3(20, 6, 30),
            new THREE.Vector3(30, 4, 30)
        ]
    ]


    function redrawBezierSurface() {
        scene.remove(bezierSurface);

        let basicBezierModel = [];

        for (let i = 0; i < bezierControlPoints.length; i++) {
            const bezier = new THREE.CubicBezierCurve3(
                bezierControlPoints[i][0],
                bezierControlPoints[i][1],
                bezierControlPoints[i][2],
                bezierControlPoints[i][3]
            )
            basicBezierModel.push(bezier.getPoints(bezierCurveDivisions));
        }


        let bezierCurvesVertices = [];
        for (let i = 0; i <= bezierCurveDivisions; i++) {
            const bezier = new THREE.CubicBezierCurve3(
                basicBezierModel[0][i],
                basicBezierModel[1][i],
                basicBezierModel[2][i],
                basicBezierModel[3][i]
            )

            bezierCurvesVertices = bezierCurvesVertices.concat(bezier.getPoints(bezierCurveDivisions));
        }

        const bezierSurfaceVertices = bezierCurvesVertices;
        const bezierSurfaceFaces = [];

        let v1, v2, v3;
        for (let i = 0; i < bezierCurveDivisions; i++) {
            for (let j = 0; j < bezierCurveDivisions; j++) {
                v1 = i * (bezierCurveDivisions + 1) + j;
                v2 = (i+1) * (bezierCurveDivisions + 1) + j;
                v3 = i * (bezierCurveDivisions + 1) + (j+1);
                bezierSurfaceFaces.push( new THREE.Face3(v1, v2, v3) );
                
                v1 = (i+1) * (bezierCurveDivisions + 1) + j;
                v2 = (i+1) * (bezierCurveDivisions + 1) + (j+1);
                v3 = i * (bezierCurveDivisions + 1) + (j+1);
                bezierSurfaceFaces.push( new THREE.Face3(v1, v2, v3) );
            }
        }

        bezierSurfaceGeometry = new THREE.Geometry();
        bezierSurfaceGeometry.vertices = bezierSurfaceVertices;
        bezierSurfaceGeometry.faces = bezierSurfaceFaces;
        bezierSurfaceGeometry.computeFaceNormals();
        bezierSurfaceGeometry.computeVertexNormals();
        bezierSurfaceMaterial = new THREE.MeshLambertMaterial({color: 0xff4444, wireframe: true});
        bezierSurface = new THREE.Mesh(bezierSurfaceGeometry, bezierSurfaceMaterial);
        bezierSurface.material.side = THREE.DoubleSide;
        scene.add(bezierSurface);
    }

    redrawBezierSurface();

    const ambientLight = new THREE.AmbientLight(0x0c0c0c);
    scene.add(ambientLight);

    function renderScene() {
        renderer.render(scene, camera);
    }
    

    function update() {
        cameraControls.update();
        requestAnimationFrame(update);
        renderScene();
        const axis = $('#X').is(':checked') ? 'x' : 'y';
        bezierSurface.rotation[axis] += 0.01;
    }

    update();

    $('#main').append(renderer.domElement);
    renderScene();
});