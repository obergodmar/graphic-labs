"use strict";
var Point3d = /** @class */ (function () {
    function Point3d(x, y, z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    return Point3d;
}());
