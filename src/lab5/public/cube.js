"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Cube = /** @class */ (function (_super) {
    __extends(Cube, _super);
    function Cube(x, y, z, size) {
        var _this = _super.call(this, x, y, z) || this;
        size *= 0.5;
        _this.vertices = [
            new Point3d(x - size, y - size, z - size),
            new Point3d(x + size, y - size, z - size),
            new Point3d(x + size, y + size, z - size),
            new Point3d(x - size, y + size, z - size),
            new Point3d(x - size, y - size, z + size),
            new Point3d(x + size, y - size, z + size),
            new Point3d(x + size, y + size, z + size),
            new Point3d(x - size, y + size, z + size)
        ];
        _this.faces = [
            [0, 1, 2, 3],
            [0, 4, 5, 1],
            [1, 5, 6, 2],
            [3, 2, 6, 7],
            [0, 3, 7, 4],
            [4, 7, 6, 5]
        ];
        return _this;
    }
    Cube.prototype.rotateX = function (radian) {
        var cosine = Math.cos(radian);
        var sine = Math.sin(radian);
        for (var index = this.vertices.length - 1; index > -1; --index) {
            var p = this.vertices[index];
            var y = (p.y - this.y) * cosine - (p.z - this.z) * sine;
            var z = (p.y - this.y) * sine + (p.z - this.z) * cosine;
            p.y = y + this.y;
            p.z = z + this.z;
        }
    };
    Cube.prototype.rotateY = function (radian) {
        var cosine = Math.cos(radian);
        var sine = Math.sin(radian);
        for (var index = this.vertices.length - 1; index > -1; --index) {
            var p = this.vertices[index];
            var x = (p.z - this.z) * sine + (p.x - this.x) * cosine;
            var z = (p.z - this.z) * cosine - (p.x - this.x) * sine;
            p.x = x + this.x;
            p.z = z + this.z;
        }
    };
    return Cube;
}(Point3d));
