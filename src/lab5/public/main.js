"use strict";
var canvas = document.querySelector('canvas');
if (!canvas) {
    canvas = document.createElement('canvas');
    document.body.appendChild(canvas);
}
var context = canvas.getContext('2d');
if (!context) {
    throw new Error('2D не поддерживается!');
}
var pointer = new Point2d(0, 0);
var size = 100;
var cube = new Cube(0, 0, 400, size);
var height = document.documentElement.clientHeight;
var width = document.documentElement.clientWidth;
var job = function (points3d, width, height) {
    var points2d = new Array(points3d.length);
    var focalLength = 200;
    for (var index = points3d.length - 1; index > -1; --index) {
        var p = points3d[index];
        var x = p.x * (focalLength / p.z) + width * 0.5;
        var y = p.y * (focalLength / p.z) + height * 0.5;
        points2d[index] = new Point2d(x, y);
    }
    return points2d;
};
var loop = function () {
    window.requestAnimationFrame(loop);
    height = document.documentElement.clientHeight;
    width = document.documentElement.clientWidth;
    context.canvas.height = height;
    context.canvas.width = width;
    context.fillStyle = 'transparent';
    context.fillRect(0, 0, width, height);
    context.strokeStyle = 'red';
    context.lineWidth = 2;
    cube.rotateX(pointer.y * 0.0001);
    cube.rotateY(-pointer.x * 0.0001);
    context.fillStyle = 'transparent';
    var vertices = job(cube.vertices, width, height);
    for (var index = cube.faces.length - 1; index > -1; --index) {
        var face = cube.faces[index];
        var p1 = cube.vertices[face[0]];
        var p2 = cube.vertices[face[1]];
        var p3 = cube.vertices[face[2]];
        var v1 = new Point3d(p2.x - p1.x, p2.y - p1.y, p2.z - p1.z);
        var v2 = new Point3d(p3.x - p1.x, p3.y - p1.y, p3.z - p1.z);
        var n = new Point3d(v1.y * v2.z - v1.z * v2.y, v1.z * v2.x - v1.x * v2.z, v1.x * v2.y - v1.y * v2.x);
        if (-p1.x * n.x + -p1.y * n.y + -p1.z * n.z <= 0) {
            context.beginPath();
            context.moveTo(vertices[face[0]].x, vertices[face[0]].y);
            context.lineTo(vertices[face[1]].x, vertices[face[1]].y);
            context.lineTo(vertices[face[2]].x, vertices[face[2]].y);
            context.lineTo(vertices[face[3]].x, vertices[face[3]].y);
            context.closePath();
            context.fill();
            context.stroke();
        }
    }
};
loop();
window.addEventListener('wheel', function (event) {
    var deltaY = event.deltaY;
    if (deltaY > 0 && size > 100) {
        size -= 50;
    }
    if (deltaY < 0 && size < 400) {
        size += 50;
    }
    cube = new Cube(0, 0, 400, size);
});
window.addEventListener('click', function (event) {
    pointer.x = event.pageX - width * 0.5;
    pointer.y = event.pageY - height * 0.5;
});
