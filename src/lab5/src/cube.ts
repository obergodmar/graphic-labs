class Cube extends Point3d {
	public vertices: Point3d[];
	public faces: number[][];
	constructor(x: number, y: number, z: number, size: number) {
		super(x, y, z);
		size *= 0.5;
		this.vertices = [
			new Point3d(x - size, y - size, z - size),
			new Point3d(x + size, y - size, z - size),
			new Point3d(x + size, y + size, z - size),
			new Point3d(x - size, y + size, z - size),
			new Point3d(x - size, y - size, z + size),
			new Point3d(x + size, y - size, z + size),
			new Point3d(x + size, y + size, z + size),
			new Point3d(x - size, y + size, z + size)
		];
		this.faces = [
			[0, 1, 2, 3],
			[0, 4, 5, 1],
			[1, 5, 6, 2],
			[3, 2, 6, 7],
			[0, 3, 7, 4],
			[4, 7, 6, 5]
		];
	}

	public rotateX(radian: number) {
		const cosine = Math.cos(radian);
		const sine = Math.sin(radian);

		for (let index = this.vertices.length - 1; index > -1; --index) {
			const p = this.vertices[index];
			const y = (p.y - this.y) * cosine - (p.z - this.z) * sine;
			const z = (p.y - this.y) * sine + (p.z - this.z) * cosine;
			p.y = y + this.y;
			p.z = z + this.z;
		}
	}

	public rotateY(radian: number) {
		const cosine = Math.cos(radian);
		const sine = Math.sin(radian);

		for (let index = this.vertices.length - 1; index > -1; -- index) {
			const p = this.vertices[index];
			const x = (p.z - this.z) * sine + (p.x - this.x) * cosine;
			const z = (p.z - this.z) * cosine - (p.x - this.x) * sine;
			p.x = x + this.x;
			p.z = z + this.z;
		}
	}
}
