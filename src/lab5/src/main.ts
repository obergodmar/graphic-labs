let canvas = document.querySelector('canvas');
if (!canvas) {
	canvas = document.createElement('canvas');
	document.body.appendChild(canvas);
}

const context = canvas.getContext('2d');
if (!context) {
	throw new Error('2D не поддерживается!');
}

const pointer = new Point2d(0, 0);

let size = 100;
let cube = new Cube(0, 0, 400, size);

let height = document.documentElement.clientHeight;
let width = document.documentElement.clientWidth;

const job = (points3d: Point3d[], width: number, height: number) => {
	const points2d = new Array(points3d.length);
	let focalLength = 200;
	for (let index = points3d.length - 1; index > -1; -- index) {
		const p = points3d[index];
		const x = p.x * (focalLength / p.z) + width * 0.5;
		const y = p.y * (focalLength / p.z) + height * 0.5;
		points2d[index] = new Point2d(x, y);
	}

	return points2d;
}

const loop = () => {
	window.requestAnimationFrame(loop);

	height = document.documentElement.clientHeight;
	width = document.documentElement.clientWidth;

	context.canvas.height = height;
	context.canvas.width = width;

	context.fillStyle = 'transparent';
	context.fillRect(0, 0, width, height);

	context.strokeStyle = 'red';
	context.lineWidth = 2;

	cube.rotateX(pointer.y * 0.0001);
	cube.rotateY(-pointer.x * 0.0001);

	context.fillStyle = 'transparent';

	const vertices = job(cube.vertices, width, height);

	for (let index = cube.faces.length - 1; index > -1; -- index) {
		const face = cube.faces[index];
		const p1 = cube.vertices[face[0]];
		const p2 = cube.vertices[face[1]];
		const p3 = cube.vertices[face[2]];
		const v1 = new Point3d(p2.x - p1.x, p2.y - p1.y, p2.z - p1.z);
		const v2 = new Point3d(p3.x - p1.x, p3.y - p1.y, p3.z - p1.z);
		const n  = new Point3d(v1.y * v2.z - v1.z * v2.y, v1.z * v2.x - v1.x * v2.z, v1.x * v2.y - v1.y * v2.x);
		if (-p1.x * n.x + -p1.y * n.y + -p1.z * n.z <= 0) {
			context.beginPath();
			context.moveTo(vertices[face[0]].x, vertices[face[0]].y);
			context.lineTo(vertices[face[1]].x, vertices[face[1]].y);
			context.lineTo(vertices[face[2]].x, vertices[face[2]].y);
			context.lineTo(vertices[face[3]].x, vertices[face[3]].y);
			context.closePath();
			context.fill();
			context.stroke();
		}
	}
}

loop();

window.addEventListener('wheel', event => {
	const {deltaY} = event;
	if (deltaY > 0 && size > 100) {
		size -= 50;
	}
	if (deltaY < 0 && size < 400) {
		size += 50;
	}
	cube = new Cube(0, 0, 400, size);
})

window.addEventListener('click', event => {
	pointer.x = event.pageX - width * 0.5;
	pointer.y = event.pageY - height * 0.5;
})
