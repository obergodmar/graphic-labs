"use strict";
var scene;
var camera;
var mesh;
var render;
var ambientLight;
var light;
var keyboard = {};
var player = {
    height: 1.8,
    speed: 0.2,
    turnSpeed: Math.PI * 0.02
};
var init = function () {
    var oldRender = document.querySelector('canvas');
    if (oldRender) {
        document.body.removeChild(oldRender);
    }
    var height = window.innerHeight;
    var width = window.innerWidth;
    var aspectRatio = width / height;
    scene = new THREE.Scene();
    camera = new THREE.PerspectiveCamera(90, aspectRatio, 0.1, 1000);
    mesh = new THREE.Mesh(new THREE.BoxGeometry(1, 1, 1), new THREE.MeshPhongMaterial({
        color: 0xff4444,
        wireframe: false
    }));
    mesh.position.y += 1;
    mesh.receiveShadow = true;
    mesh.castShadow = true;
    scene.add(mesh);
    ambientLight = new THREE.AmbientLight(0xffffff, 0.2);
    scene.add(ambientLight);
    light = new THREE.PointLight(0xffffff, 0.8, 18);
    light.position.set(-3, 6, -3);
    light.castShadow = true;
    light.shadow.camera.near = 0.1;
    light.shadow.camera.far = 25;
    scene.add(light);
    camera.position.set(0, 1.2, -5);
    camera.lookAt(new THREE.Vector3(0, 0, 0));
    render = new THREE.WebGLRenderer({ antialias: true, alpha: true });
    render.setSize(width, height);
    render.shadowMap.enabled = true;
    render.shadowMap.type = THREE.BasicShadowMap;
    document.body.appendChild(render.domElement);
};
var animate = function () {
    requestAnimationFrame(animate);
    mesh.rotation.x += 0.01;
    mesh.rotation.y += 0.02;
    if (keyboard[87]) { // W key
        camera.position.x -= Math.sin(camera.rotation.y) * player.speed;
        camera.position.z -= -Math.cos(camera.rotation.y) * player.speed;
    }
    if (keyboard[83]) { // S key
        camera.position.x += Math.sin(camera.rotation.y) * player.speed;
        camera.position.z += -Math.cos(camera.rotation.y) * player.speed;
    }
    if (keyboard[65]) { // A key
        camera.position.x += Math.sin(camera.rotation.y + Math.PI / 2) * player.speed;
        camera.position.z += -Math.cos(camera.rotation.y + Math.PI / 2) * player.speed;
    }
    if (keyboard[68]) { // D key
        camera.position.x += Math.sin(camera.rotation.y - Math.PI / 2) * player.speed;
        camera.position.z += -Math.cos(camera.rotation.y - Math.PI / 2) * player.speed;
    }
    if (keyboard[37]) { // left arrow key
        camera.rotation.y -= player.turnSpeed;
    }
    if (keyboard[39]) { // right arrow key
        camera.rotation.y += player.turnSpeed;
    }
    render.render(scene, camera);
};
function keyDown(event) {
    keyboard[event.keyCode] = true;
}
function keyUp(event) {
    keyboard[event.keyCode] = false;
}
window.addEventListener('keydown', keyDown);
window.addEventListener('keyup', keyUp);
window.addEventListener('resize', init);
window.onload = function () {
    init();
    animate();
};
