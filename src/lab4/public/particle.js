"use strict";
var Particle = /** @class */ (function () {
    function Particle(x, y, vx, vy) {
        this.x = x;
        this.y = y;
        this.vx = vx;
        this.vy = vy;
    }
    Particle.random = function () {
        var random = function (min, max) {
            return Math.floor(min + Math.random() * (max - min));
        };
        var x = random(0.0, window.innerWidth / 1.1);
        var y = random(0.0, window.innerHeight / 1.1);
        var vx = random(-2.80, 4.0);
        var vy = random(-2.80, 4.0);
        return new Particle(x, y, vx, vy);
    };
    Particle.prototype.update = function () {
        var innerWidth = window.innerWidth, innerHeight = window.innerHeight;
        this.x += this.vx;
        this.y += this.vy;
        if (this.x < 0) {
            this.vx *= -1;
        }
        if (this.y < 0) {
            this.vy *= -1;
        }
        if (this.x > innerWidth) {
            this.vx *= -1;
        }
        if (this.y > innerHeight) {
            this.vy *= -1;
        }
    };
    return Particle;
}());
