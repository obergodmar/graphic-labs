"use strict";
var Point = function (x, y) { return ({ x: x, y: y }); };
var Vector = function (x, y, z) { return ({ x: x, y: y, z: z }); };
var pointCross = function (a, b) { return (Vector(a.y - b.y, b.x - a.x, a.x * b.y - a.y * b.x)); };
var cross = function (u, v) {
    var z = u.x * v.y - u.y * v.x;
    return Point((u.y * v.z - u.z * v.y) / z, (u.z * v.x - u.x * v.z) / z);
};
var dot = function (u, v) { return u.x * v.x + u.y * v.y + u.z * v.z; };
var mask = [0, 1, 2, 2, 4, 0, 4, 4, 8, 1, 0, 2, 8, 1, 8, 0];
var tab1 = [4, 3, 0, 3, 1, 4, 0, 3, 2, 2, 4, 2, 1, 1, 0, 4];
var tab2 = [4, 0, 1, 1, 2, 4, 2, 2, 3, 0, 4, 1, 3, 0, 3, 4];
var Clipper = /** @class */ (function () {
    function Clipper(a, b) {
        if (a.x < b.x) {
            this.xMin = a.x;
            this.xMax = b.x;
        }
        else {
            this.xMin = b.x;
            this.xMax = a.x;
        }
        if (a.y < b.y) {
            this.yMin = a.y;
            this.yMax = b.y;
        }
        else {
            this.yMin = b.y;
            this.yMax = a.y;
        }
        this.x = [
            Vector(this.xMin, this.yMin, 1),
            Vector(this.xMax, this.yMin, 1),
            Vector(this.xMax, this.yMax, 1),
            Vector(this.xMin, this.yMax, 1)
        ];
        this.e = [
            Vector(0, 1, -this.yMin),
            Vector(1, 0, -this.xMax),
            Vector(0, 1, -this.yMax),
            Vector(1, 0, -this.xMin)
        ];
    }
    Clipper.prototype.code = function (point) {
        var c = 0;
        if (point.x < this.xMin) {
            c = 8;
        }
        else if (point.x > this.xMax) {
            c = 2;
        }
        if (point.y < this.yMin) {
            c |= 1;
        }
        else if (point.y > this.yMax) {
            c |= 4;
        }
        return c;
    };
    ;
    // @ts-ignore
    Clipper.prototype.clipLine = function (xA, xB) {
        var cA = this.code(xA);
        var cB = this.code(xB);
        if ((cA | cB) == 0) {
            return [0, xA, xB];
        }
        if ((cA & cB) != 0) {
            return [-1, xA, xB];
        }
        var p = pointCross(xA, xB);
        var c = 0;
        for (var k = 0; k < 4; k++) {
            if (dot(p, this.x[k]) <= 0) {
                c |= (1 << k);
            }
        }
        if (c == 0 || c == 15) {
            return [-1, xA, xB];
        }
        var i = tab1[c];
        var j = tab2[c];
        var resultXA, resultXB;
        if (cA != 0 && cB != 0) {
            resultXA = cross(p, this.e[i]);
            resultXB = cross(p, this.e[j]);
            return [3, resultXA, resultXB];
        }
        else {
            if (cA == 0) {
                if ((cB & mask[c]) == 0) {
                    resultXB = cross(p, this.e[i]);
                }
                else {
                    resultXB = cross(p, this.e[j]);
                }
                return [2, xA, resultXB];
            }
            else if (cB == 0) {
                if ((cA & mask[c]) == 0) {
                    resultXA = cross(p, this.e[i]);
                }
                else {
                    resultXA = cross(p, this.e[j]);
                }
                return [1, resultXA, xB];
            }
        }
    };
    return Clipper;
}());
