"use strict";
var canvas = document.getElementById('main');
var ctx = canvas.getContext('2d');
var factor = 1;
var frameHeight = 220;
var frameWidth = 220;
var frameLeft = (innerWidth - frameWidth) * 0.5;
var frameRight = (innerWidth + frameWidth) * 0.5;
var frameTop = (innerHeight - frameHeight) * 0.5;
var frameBottom = (innerHeight + frameHeight) * 0.5;
var createRandomLines = function (numberOfLines) {
    var lines = [];
    for (var i = 0; i < numberOfLines; i++) {
        var a = Particle.random();
        var b = Particle.random();
        lines.push([a, b]);
    }
    return lines;
};
var lines = createRandomLines(5);
;
function handleParticlesInput() {
    var particlesInput = document.getElementById('particle-number');
    var particlesNumber = Number(particlesInput.value);
    if (particlesNumber < 1) {
        particlesInput.value = "1";
        particlesNumber = 1;
    }
    lines = createRandomLines(particlesNumber);
}
var clear = function () { return ctx && ctx.clearRect(0, 0, canvas.width, canvas.height); };
var draw = function () {
    var innerWidth = window.innerWidth, innerHeight = window.innerHeight;
    if (!ctx) {
        return;
    }
    canvas.width = innerWidth * factor;
    canvas.height = innerHeight * factor;
    canvas.style.width = innerWidth + "px";
    canvas.style.height = innerHeight + "px";
    ctx.scale(factor, factor);
    ctx.save();
    ctx.shadowBlur = 10;
    ctx.shadowColor = 'rgb(200,200,200)';
    ctx.fillStyle = 'white';
    ctx.beginPath();
    ctx.rect(frameLeft, frameTop, frameWidth, frameHeight);
    ctx.fill();
    ctx.restore();
    var clipper = new Clipper(Point(frameLeft, frameTop), Point(frameRight, frameBottom));
    for (var i = 0; i < lines.length; i++) {
        var _a = lines[i], a = _a[0], b = _a[1];
        ctx.save();
        ctx.strokeStyle = 'rgb(180,180,180)';
        ctx.beginPath();
        ctx.moveTo(a.x, a.y);
        ctx.lineTo(b.x, b.y);
        ctx.stroke();
        ctx.restore();
        var _b = clipper.clipLine(a, b), result = _b[0], newA = _b[1], newB = _b[2];
        if (result < 0) {
            continue;
        }
        ctx.save();
        ctx.beginPath();
        ctx.moveTo(newA.x, newA.y);
        ctx.lineTo(newB.x, newB.y);
        ctx.strokeStyle = 'rgb(176, 0, 0)';
        ctx.stroke();
        ctx.restore();
    }
};
var update = function () {
    for (var i = 0; i < lines.length; i++) {
        var _a = lines[i], a = _a[0], b = _a[1];
        a.update();
        b.update();
    }
};
var loop = function () {
    clear();
    draw();
    update();
    queue();
};
var queue = function () { return window.requestAnimationFrame(loop); };
loop();
function dragRect() {
    var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
    canvas.onmousedown = dragMouseDown;
    function dragMouseDown(e) {
        e = e || window.event;
        e.preventDefault();
        canvas.style.cursor = 'move';
        String(pos3 = e.clientX);
        pos4 = e.clientY;
        document.onmouseup = closeDragElement;
        document.onmousemove = elementDrag;
    }
    function elementDrag(e) {
        e = e || window.event;
        e.preventDefault();
        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;
        frameLeft -= pos1;
        frameRight -= pos1;
        frameTop -= pos2;
        frameBottom -= pos2;
    }
    function closeDragElement() {
        canvas.style.cursor = 'default';
        document.onmouseup = null;
        document.onmousemove = null;
    }
}
dragRect();
var toolbox = document.getElementById('toolbox');
dragToolbox(toolbox);
function dragToolbox(toolbox) {
    var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
    var toolboxHeader = document.getElementById(toolbox.id + "-header");
    if (toolboxHeader) {
        toolboxHeader.onmousedown = dragMouseDown;
    }
    else {
        toolbox.onmousedown = dragMouseDown;
    }
    function dragMouseDown(e) {
        e = e || window.event;
        e.preventDefault();
        pos3 = e.clientX;
        pos4 = e.clientY;
        document.onmouseup = closeDragElement;
        document.onmousemove = elementDrag;
    }
    function elementDrag(e) {
        e = e || window.event;
        e.preventDefault();
        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;
        toolbox.style.top = (toolbox.offsetTop - pos2) + "px";
        toolbox.style.left = (toolbox.offsetLeft - pos1) + "px";
    }
    function closeDragElement() {
        document.onmouseup = null;
        document.onmousemove = null;
    }
}
