const canvas = document.getElementById('main') as HTMLCanvasElement;
const ctx = canvas.getContext('2d');
const factor = 1;
const frameHeight = 220;
const frameWidth = 220;

let frameLeft = (innerWidth - frameWidth) * 0.5;
let frameRight = (innerWidth + frameWidth) * 0.5;
let frameTop = (innerHeight - frameHeight) * 0.5;
let frameBottom = (innerHeight + frameHeight) * 0.5;

type Lines = Particle[][];

const createRandomLines = (numberOfLines: number): Lines => {
	let lines = [];

	for(let i = 0; i < numberOfLines; i++) {
		let a = Particle.random();
		let b = Particle.random();

		lines.push([a, b]);
	}

	return lines;
};

let lines = createRandomLines(5);;

function handleParticlesInput() {
	const particlesInput = document.getElementById('particle-number') as HTMLInputElement;
	let particlesNumber = Number(particlesInput.value);

	if (particlesNumber < 1) {
		particlesInput.value = "1";
		particlesNumber = 1;
	}

	lines = createRandomLines(particlesNumber);
}

const clear = () => ctx && ctx.clearRect(0, 0, canvas.width, canvas.height);

const draw = () => {
	const {innerWidth, innerHeight} = window;
	if (!ctx) {
		return;
	}
	canvas.width = innerWidth * factor;
	canvas.height = innerHeight * factor;
	canvas.style.width = `${innerWidth}px`;
	canvas.style.height = `${innerHeight}px`;
	ctx.scale(factor, factor);

	ctx.save();
	ctx.shadowBlur = 10;
	ctx.shadowColor = 'rgb(200,200,200)';
	ctx.fillStyle = 'white';
	ctx.beginPath();
	ctx.rect(frameLeft, frameTop, frameWidth, frameHeight);
	ctx.fill();
	ctx.restore();

	const clipper = new Clipper(
		Point(frameLeft, frameTop),
		Point(frameRight, frameBottom)
	);

	for (let i = 0; i < lines.length; i++) {
		let [a, b] = lines[i];

		ctx.save();
		ctx.strokeStyle='rgb(180,180,180)';
		ctx.beginPath();
		ctx.moveTo(a.x, a.y);
		ctx.lineTo(b.x, b.y);
		ctx.stroke();
		ctx.restore();

		const [result, newA, newB] = clipper.clipLine(a, b);

		if (result < 0) {
			continue;
		}

		ctx.save();
		ctx.beginPath();
		ctx.moveTo(newA.x, newA.y);
		ctx.lineTo(newB.x, newB.y);
		ctx.strokeStyle = 'rgb(176, 0, 0)';
		ctx.stroke();
		ctx.restore();
	}
};

const update = () => {
	for (let i = 0; i < lines.length; i++) {
		const [a, b] = lines[i];
		a.update();
		b.update();
	}
};

const loop = () => {
	clear();
	draw();
	update();
	queue();
};

const queue = () =>	window.requestAnimationFrame(loop);

loop();

function dragRect() {
	let pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;

	canvas.onmousedown = dragMouseDown;

	function dragMouseDown(e: MouseEvent) {
		e = e || window.event;
		e.preventDefault();
		canvas.style.cursor = 'move';
		String(pos3 = e.clientX);
		pos4 = e.clientY;
		document.onmouseup = closeDragElement;
		document.onmousemove = elementDrag;
	}

	function elementDrag(e: MouseEvent) {
		e = e || window.event;
		e.preventDefault();
		pos1 = pos3 - e.clientX;
		pos2 = pos4 - e.clientY;
		pos3 = e.clientX;
		pos4 = e.clientY;

		frameLeft -= pos1;
		frameRight -= pos1;
		frameTop -= pos2;
		frameBottom -= pos2;
	}

	function closeDragElement() {
		canvas.style.cursor = 'default';
		document.onmouseup = null;
		document.onmousemove = null;
	}
}

dragRect();

const toolbox = document.getElementById('toolbox') as HTMLElement;
dragToolbox(toolbox);

function dragToolbox(toolbox: HTMLElement) {
	let pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
	const toolboxHeader = document.getElementById(toolbox.id + "-header");
	if (toolboxHeader) {
		toolboxHeader.onmousedown = dragMouseDown;
	} else {
		toolbox.onmousedown = dragMouseDown;
	}

	function dragMouseDown(e: MouseEvent) {
		e = e || window.event;
		e.preventDefault();
		pos3 = e.clientX;
		pos4 = e.clientY;
		document.onmouseup = closeDragElement;
		document.onmousemove = elementDrag;
	}

	function elementDrag(e: MouseEvent) {
		e = e || window.event;
		e.preventDefault();
		pos1 = pos3 - e.clientX;
		pos2 = pos4 - e.clientY;
		pos3 = e.clientX;
		pos4 = e.clientY;
		toolbox.style.top = (toolbox.offsetTop - pos2) + "px";
		toolbox.style.left = (toolbox.offsetLeft - pos1) + "px";
	}

	function closeDragElement() {
		document.onmouseup = null;
		document.onmousemove = null;
	}
}
