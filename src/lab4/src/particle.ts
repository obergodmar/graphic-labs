class Particle {
	x: number;
	y: number;
	vx: number;
	vy: number;
	constructor(x: number, y: number, vx: number, vy: number) {
		this.x = x;
		this.y = y;
		this.vx = vx;
		this.vy = vy;
	}

	static random(): Particle {
		const random = function(min: number, max: number) {
			return Math.floor(min + Math.random() * (max - min));
		};

		const x = random(0.0, window.innerWidth / 1.1);
		const y = random(0.0, window.innerHeight / 1.1);

		const vx = random(-2.80, 4.0);
		const vy = random(-2.80, 4.0);

		return new Particle(x, y, vx, vy);
	}

	update(): void {
		const {innerWidth, innerHeight} = window;

		this.x += this.vx;
		this.y += this.vy;

		if (this.x < 0) {
			this.vx *= -1;
		}
		if (this.y < 0) {
			this.vy *= -1;
		}
		if (this.x > innerWidth) {
			this.vx *= -1;
		}
		if (this.y > innerHeight) {
			this.vy *= -1;
		}
	}
}
